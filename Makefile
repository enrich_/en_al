.PHONY: help check test lint compile tidy build publish build-localdb start generate

help: ## Show this help
	@echo "Help"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "    \033[36m%-20s\033[93m %s\n", $$1, $$2}'

### Code validation
check: ## Run all checks: test and lint
	@bash scripts/check.sh

test: ## Run tests for all go packages
	@bash scripts/checks/test.sh

coverage: test ## Run tests and open coverage report
	@go tool cover -html=.test_coverage.txt

lint: ## Run lint on the codebase, printing any style errors
	@bash scripts/checks/lint.sh

fix-tidy: ## Fix go.mod inconsistency
	@bash scripts/local/fix-tidy.sh

compile: ## Compile the binary
	@bash scripts/compile.sh

build: ## Build the image
	@bash scripts/build.sh

build-localdb: ## Build the local image for the database
	@bash scripts/build-localdb.sh user-manager-mysql

publish: ## Publish the image
	@bash scripts/publish.sh

generate: ## Generates read all generates references and executes them 
	@go generate ./...

install-tools: ## Install external tools
	@bash scripts/install-tools.sh

install-deps: ## Prefetch deps to ensure required versions are downloaded
	@GO111MODULE=on go mod tidy
	@GO111MODULE=on go mod verify
	@GO111MODULE=on go mod vendor

start-api: ## Starts rest api locally
	@bash scripts/local/start-api.sh $(PWD)/cmd/user-manager

start-webapp: ## Starts webapp locally
	@bash scripts/local/start-webapp.sh $(PWD)/cmd/webapp

start-deps: ## Starts deps locally
	@bash scripts/local/start-deps.sh

start: ## Starts services locally
	@$(MAKE) start-deps
	# TODO: Improve: use wait for it
	@$(MAKE) start-api
	@$(MAKE) start-webapp

