package testutil

import (
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func ReadTestData(t *testing.T, filename string) io.ReadCloser {
	t.Helper()

	f, err := os.Open(filepath.Join("testdata", filename))
	if err != nil {
		t.Fatalf("open testdata file error: %v", err)
	}
	return f
}

func DecodeTestData(t *testing.T, rc io.ReadCloser, v interface{}) {
	t.Helper()
	defer rc.Close()

	require.NoError(t, json.NewDecoder(rc).Decode(v))
}
