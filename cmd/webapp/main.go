package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/enrich_/en_al.git/internal/authorizer/oauth2provider"
	"bitbucket.org/enrich_/en_al.git/internal/webapp"
	"bitbucket.org/enrich_/en_al.git/pkg/httpserver"
	"bitbucket.org/enrich_/en_al.git/pkg/restusermanager"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func init() {
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(os.Stdout)
}

func main() {
	logrus.Info("Initializing webapp...")

	cfg, err := getConfig()
	if err != nil {
		logrus.Panicf("Failed to get env config: %s", err)
	}

	googleAuthorizer := oauth2provider.NewGoogleAuthorizer(cfg.GoogleOAuth2)

	client := http.Client{Timeout: time.Second * 5}
	userManagerClient := restusermanager.NewClient(cfg.UserManager, &client)

	// Webapp
	logrus.Info("Starting webapp HTTP Server...")
	webAppRouter := mux.NewRouter()
	webSrv := webapp.NewServer(googleAuthorizer, userManagerClient, cfg.WebAPP.SessionSecret, cfg.CookieDomain)
	webSrv.AddRoutes(webAppRouter)
	webAppHTTPServer := httpserver.Start(webAppRouter, cfg.WebAPP.HTTPServerPort)

	logrus.Info("Webapp is running")

	// TODO: improve by creating a interface for the Closer tasks...
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)
	<-exit

	logrus.Info("Terminating...")
	timeoutCtx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	webAppHTTPServer.Stop(timeoutCtx)

	logrus.Info("Terminated")
}
