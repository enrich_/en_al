package main

import (
	"fmt"

	"bitbucket.org/enrich_/en_al.git/internal/authorizer/oauth2provider"
	"bitbucket.org/enrich_/en_al.git/internal/webapp"
	"bitbucket.org/enrich_/en_al.git/pkg/restusermanager"
	"github.com/kelseyhightower/envconfig"
)

type config struct {
	GoogleOAuth2 oauth2provider.Config
	UserManager  restusermanager.Config

	CookieDomain string `required:"true"`

	WebAPP webapp.Config
}

func getConfig() (*config, error) {
	cfg := config{}

	if err := envconfig.Process("webapp", &cfg); err != nil {
		return nil, fmt.Errorf("failed to read the env: %v", err)
	}
	return &cfg, nil
}
