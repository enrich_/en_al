package webapp

import "github.com/gorilla/sessions"

type userSession struct {
	Authenticated bool
	Email         string
	ID            string
	GoogleJWT     string
}

// getUser returns a user from session
func getUserSession(s *sessions.Session) userSession {
	val := s.Values["user"]
	user, ok := val.(userSession)
	if !ok {
		return userSession{Authenticated: false}
	}
	return user
}
