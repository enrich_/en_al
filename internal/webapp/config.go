package webapp

// Config defines the confgiguation for the webapp http server
type Config struct {
	HTTPServerPort uint32 `default:"8080"`
	SessionSecret  string `required:"true"`
}
