package customerrors

type BaseErr struct {
	Err error
}

func (e BaseErr) Error() string {
	return e.Err.Error()
}

type ErrMalformed struct {
	BaseErr
}

func NewErrMalformed(err error) ErrMalformed {
	return ErrMalformed{BaseErr{Err: err}}
}

type ErrConflict struct {
	BaseErr
}

func NewErrConflict(err error) ErrConflict {
	return ErrConflict{BaseErr{Err: err}}
}

type ErrInternal struct {
	BaseErr
}

func NewErrInternal(err error) ErrInternal {
	return ErrInternal{BaseErr{Err: err}}
}

type ErrResourceNotFound struct {
	BaseErr
}

func NewErrResourceNotFound(err error) ErrResourceNotFound {
	return ErrResourceNotFound{BaseErr{Err: err}}
}

type ErrForbidden struct {
	BaseErr
}

func NewErrForbidden(err error) ErrForbidden {
	return ErrForbidden{BaseErr{Err: err}}
}

// IsCustomErr checks if the provided err is a custom error
func IsCustomErr(err error) bool {
	switch err.(type) {
	case ErrForbidden, ErrMalformed, ErrResourceNotFound, ErrConflict, ErrInternal:
		return true
	}
	return false
}
