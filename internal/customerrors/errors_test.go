package customerrors

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCustomErrors(t *testing.T) {
	t.Run("Should validate specific errors", func(t *testing.T) {
		malformedErr := NewErrMalformed(errors.New("bad input"))
		assert.True(t, IsCustomErr(malformedErr))

		conflictErr := NewErrConflict(errors.New("entity already exists"))
		assert.True(t, IsCustomErr(conflictErr))

		internalErr := NewErrInternal(errors.New("database id down"))
		assert.True(t, IsCustomErr(internalErr))

		forbiddenErr := NewErrForbidden(errors.New("unauthorized"))
		assert.True(t, IsCustomErr(forbiddenErr))

		resourceNotFoundErr := NewErrResourceNotFound(errors.New("entity does not exist"))
		assert.True(t, IsCustomErr(resourceNotFoundErr))

		err := errors.New("generic error")
		assert.False(t, IsCustomErr(err))
	})

	t.Run("Should check error as string", func(t *testing.T) {
		malformedErr := NewErrMalformed(errors.New("bad input"))
		assert.Equal(t, malformedErr.Error(), "bad input")
	})
}
