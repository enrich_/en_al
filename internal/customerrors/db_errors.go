package customerrors

import (
	"errors"

	"bitbucket.org/enrich_/en_al.git/internal/repository"
)

// FromDatabaseErr generates a new custom error based on the database error
func FromDatabaseErr(err error) error {
	if errors.Is(err, repository.ErrNotFound) {
		return NewErrResourceNotFound(repository.ErrNotFound)
	}
	if errors.Is(err, repository.ErrAlreadyExists) {
		return NewErrConflict(repository.ErrAlreadyExists)
	}
	return NewErrInternal(repository.ErrDatabaseServer)
}
