package repository

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// TxSQLOptions defines all SQL transaction options
type TxSQLOptions struct {
	isolationLvl sql.IsolationLevel
}

// TxSQLOption is a transaction SQL configuration option
type TxSQLOption func(*TxSQLOptions)

// WithSQLIsolationLevel can be used to change the isolation level of the transaction
func WithSQLIsolationLevel(lvl sql.IsolationLevel) TxSQLOption {
	return func(opts *TxSQLOptions) {
		opts.isolationLvl = lvl
	}
}

// TxSQLFn is a function that will be called with an initialized `Transaction` object
// that can be used for executing statements and queries against a database.
type TxSQLFn func(tx *sqlx.Tx) error

// TxSQLBuilder is used to build a new transaction
type TxSQLBuilder struct {
	db             *sqlx.DB
	isolationLevel sql.IsolationLevel
}

// NewSQLTransaction creates a new sql transaction builder
func NewSQLTransaction(db *sqlx.DB, txOpts ...TxSQLOption) *TxSQLBuilder {
	opts := &TxSQLOptions{
		isolationLvl: sql.LevelDefault,
	}

	for _, txOptSetter := range txOpts {
		txOptSetter(opts)
	}

	return &TxSQLBuilder{
		db:             db,
		isolationLevel: opts.isolationLvl,
	}
}

// Do runs the passed function in a sql transaction
func (txb *TxSQLBuilder) Do(ctx context.Context, fn TxSQLFn) error {
	tx, err := txb.db.BeginTxx(ctx, &sql.TxOptions{
		Isolation: txb.isolationLevel,
	})
	if err != nil {
		return fmt.Errorf("create transaction: %v", err)
	}

	defer func() {
		if p := recover(); p != nil {
			// a panic occurred, rollback and re-panic
			_ = tx.Rollback()
			panic(p)
		} else if err != nil {
			// database server error: rollback transaction
			_ = tx.Rollback()
		} else {
			// we're good to go
			err = tx.Commit()
		}
	}()

	err = fn(tx)

	return err
}

// MustExecSQLTx checks guarantees that the passed function will be executed
// in a transaction
func MustExecSQLTx(ctx context.Context, conn sqlx.ExtContext, fn TxSQLFn) error {
	if tx, ok := conn.(*sqlx.Tx); ok {
		return fn(tx)
	}

	// We let it panic: if we're not able to cast the conn to either a Tx
	// or a DB, unexpected behaviour.
	// This error depends on the user provided arguments
	db, ok := conn.(*sqlx.DB)
	if !ok {
		panic("Couldn't cast sql connection sqlx.ExtContext to either *sqlx.DB or *sqlx.Tx")
	}

	return NewSQLTransaction(db).Do(ctx, fn)
}
