package repository

import (
	"errors"
)

var (
	// ErrAlreadyExists occurs when a record already exists in the database
	ErrAlreadyExists = errors.New("already exists")
	// ErrNotFound occurs when record(s) does not exist
	ErrNotFound = errors.New("not found")
	// ErrDatabaseServer occurs when a constraint is violated or the database server is outage,
	// or operating in degraded mode
	ErrDatabaseServer = errors.New("database server error")
)
