package oauth2provider

var scopes = []string{
	"https://www.googleapis.com/auth/userinfo.email",
	"https://www.googleapis.com/auth/userinfo.profile",
}

// Config defines the necessary configuration to enable Google OAuth2
type Config struct {
	RedirectURL  string `default:"http://localhost:8080/auth/google/callback"`
	ClientID     string `required:"true"`
	ClientSecret string `required:"true"`
}
