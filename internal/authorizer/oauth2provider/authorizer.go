package oauth2provider

import (
	"context"

	"golang.org/x/oauth2"
)

type authorizerType string

func (a authorizerType) String() string {
	return string(a)
}

const (
	GoogleAuthorizerType authorizerType = "GOOGLE"
)

//go:generate mockery -case underscore -output mocks -outpkg authorizermock -name Authorizer

// Authorizer defines a common contract every oauth2 client must comply with
type Authorizer interface {
	// Type returns the type of the provider in use
	Type() string
	// GetRedirectConfig returns the configuration to redirect user to the auth provider page
	GetRedirectConfig() *SignInRedirectConfig
	// Exchange exchanges a code for an access_token
	Exchange(ctx context.Context, code string) (*oauth2.Token, error)
}

// SignInRedirectConfig defines the attrs to get a new sign in redirect config
type SignInRedirectConfig struct {
	Config oauth2.Config
	State  State
}
