package generator

import (
	"encoding/base64"
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

// JWTGeneratorConfig defines config to generate jwt
type JWTGeneratorConfig struct {
	Secret string `required:"true"`
}

type JWTGenerator struct {
	secret []byte
}

// NewJWTGenerator creates a new instance JWT generator
func NewJWTGenerator(cfg JWTGeneratorConfig) JWTGenerator {
	return JWTGenerator{secret: []byte(cfg.Secret)}
}

// Generate returns a signed json web token
func (j JWTGenerator) Generate(claims map[string]interface{}) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims(claims))
	return token.SignedString(j.secret)
}

// Encode encodes a jwt token into base64
func (j JWTGenerator) Encode(token string) string {
	return base64.StdEncoding.EncodeToString([]byte(token))
}

// Decode decodes an encoded token into signed json web token
func (j JWTGenerator) Decode(encodedToken string) (string, error) {
	b, err := base64.StdEncoding.DecodeString(encodedToken)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

// Verify verifies a jwt token
func (j JWTGenerator) Verify(token string) (*jwt.Token, error) {
	result, err := jwt.Parse(token, func(jwtToken *jwt.Token) (interface{}, error) {
		if _, ok := jwtToken.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", jwtToken.Header["alg"])
		}
		return j.secret, nil
	})
	if err != nil {
		return nil, err
	}
	return result, nil
}

// IsValid verifies if a token is valid
func (j JWTGenerator) IsValid(token *jwt.Token) (map[string]interface{}, bool) {
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		return claims, true
	}
	return nil, false
}
