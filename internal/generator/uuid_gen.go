package generator

import (
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

type UUID interface {
	String() string
	Bytes() []byte
	MarshalText() ([]byte, error)
	MarshalBinary() ([]byte, error)
}

func NewUUID() UUID {
	return uuid.NewV1()
}

func ParseUUID(id string) (UUID, error) {
	return uuid.FromString(id)
}

func Bytes(id string) []byte {
	genUUID, err := uuid.FromString(id)
	if err != nil {
		logrus.Panicf("Failed to parse string(%s) to UUID: %v", id, err)
	}
	return genUUID.Bytes()
}

func String(id []byte) string {
	if len(id) == 0 {
		return ""
	}
	genUUID, err := uuid.FromBytes(id)
	if err != nil {
		logrus.Panicf("Failed to parse bytes(%s) to UUID: %v", string(id), err)
	}
	return genUUID.String()
}
