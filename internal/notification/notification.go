package notification

import "context"

//go:generate mockery -case underscore -output mocks -outpkg mailerservicemock -name MailerService

const sender = "emalvarenga96@gmail.com"

// MailerService defines a contract to end emails
type MailerService interface {
	Notify(ctx context.Context, msg MailerMessage) error
}

// MailerMessage represents all attributes necessary to send an email
type MailerMessage struct {
	Subject   string
	Body      string
	Recipient string
}
