package notification

import (
	"context"

	"github.com/mailgun/mailgun-go/v4"
)

// MailgunMailerConfig defines the configuration params for the mailer
type MailgunMailerConfig struct {
	APIKey string `required:"true"`
	Domain string `required:"true"`
}

type mailgunNotification struct {
	mg *mailgun.MailgunImpl
}

// NewMailGunNotification creates a new instance Mailgun mailer
func NewMailGunNotification(cfg MailgunMailerConfig) MailerService {
	return &mailgunNotification{
		mg: mailgun.NewMailgun(cfg.Domain, cfg.APIKey),
	}
}

func (m *mailgunNotification) Notify(ctx context.Context, msg MailerMessage) error {
	message := m.mg.NewMessage(sender, msg.Subject, msg.Body, msg.Recipient)

	_, _, err := m.mg.Send(ctx, message)
	if err != nil {
		return err
	}

	return nil
}
