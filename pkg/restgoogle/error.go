package restgoogle

import (
	"fmt"
)

// ErrorResponse in case of errors
type ErrorResponse struct {
	Err interface{} `json:"errors"`
}

func (e *ErrorResponse) Error() string {
	return fmt.Sprintf("restgoogle: %+v", e.Err)
}
