package restgoogle

// GetUserInfoResponse defines the response for request made get user information
type GetUserInfoResponse struct {
	Email      string `json:"email"`
	FamilyName string `json:"family_name"`
	Gender     string `json:"gender"`
	GivenName  string `json:"given_name"`
	ID         string `json:"id"`
	Name       string `json:"name"`
	Picture    string `json:"picture"`
}
