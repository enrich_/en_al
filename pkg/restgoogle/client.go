package restgoogle

import (
	"context"
	"net/http"
	"net/url"

	"github.com/dghubble/sling"
)

const (
	getUserInfoPath = "/oauth2/v2/userinfo"
)

//go:generate mockery -case underscore -output mocks -outpkg restgooglemock -name Client

// Client is an interface with common methods to connect with googleapis
type Client interface {
	// GetUserInfo returns user information from user's google account
	GetUserInfo(ctx context.Context, token string) (*GetUserInfoResponse, error)
}

// defaultClient represents the structure to perform authentication/authorization
type defaultClient struct {
	cfg     Config
	slinger *sling.Sling
}

// NewClient creates a new google API client
func NewClient(cfg Config, client *http.Client) Client {
	baseURL := url.URL(cfg.Address)

	slingerSvc := sling.New().Base(baseURL.String()).Client(client)

	return &defaultClient{
		cfg:     cfg,
		slinger: slingerSvc,
	}
}

func (c *defaultClient) GetUserInfo(ctx context.Context, token string) (*GetUserInfoResponse, error) {
	successResponse := GetUserInfoResponse{}
	errResponse := ErrorResponse{}

	qs := struct {
		AccessToken string `url:"access_token"`
	}{
		AccessToken: token,
	}

	req, err := c.slinger.
		New().
		QueryStruct(&qs).
		Get(getUserInfoPath).
		Request()
	if err != nil {
		return nil, err
	}

	res, err := c.slinger.Do(req.WithContext(ctx), &successResponse, &errResponse)
	if err != nil {
		return nil, err
	}

	switch res.StatusCode {
	case http.StatusOK:
		return &successResponse, nil
	default:
		return nil, &errResponse
	}
}
