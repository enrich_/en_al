package mysql

import "time"

// Config is the configuration of the MySQL DB
type Config struct {
	// Connection is the connection string passed to `sql.Open`.
	Connection string `required:"true"`
	// ConnRetrySleep is the time to wait before retrying a connection
	ConnRetrySleep time.Duration `default:"3s"`
	// ConnMaxLifetime is the maximum connection lifetime
	ConnMaxLifetime time.Duration
	// MaxIdleConns is the maxium idle connections
	MaxIdleConns uint `validate:"min=0"`
	// MaxOpenConns is the maximum open connections
	MaxOpenConns uint `validate:"min=0"`
}
