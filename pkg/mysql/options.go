package mysql

import (
	"fmt"
	"net/url"
	"strings"
)

const querySeparator = "?"

var defaultOptions = map[string]string{
	"charset":         "utf8mb4",
	"loc":             "UTC",
	"parseTime":       "True",
	"multiStatements": "True",
	"time_zone":       "'+00:00'",
}

func connWithDefaultOpts(dsn string) (string, error) {
	authorityPath, queryString := splitDSN(dsn)
	query, err := url.ParseQuery(queryString)
	if err != nil {
		return "", fmt.Errorf("parse connection string: %v", err)
	}

	query = withDefaultOpts(query)
	queryString = query.Encode()
	dsnWithDefaults := joinDSN(authorityPath, queryString)
	return dsnWithDefaults, nil
}

func splitDSN(dsn string) (authorityPath, queryString string) {
	querySeparatorIdx := strings.LastIndex(dsn, querySeparator)
	if querySeparatorIdx == -1 {
		authorityPath = dsn
	} else {
		authorityPath = dsn[0:querySeparatorIdx]
		queryString = dsn[querySeparatorIdx+1:]
	}
	return
}

func withDefaultOpts(query url.Values) url.Values {
	for option, defaultValue := range defaultOptions {
		valueList := query[option]
		if len(valueList) == 0 {
			query.Set(option, defaultValue)
		}
	}
	return query
}

func joinDSN(authorityPath, queryString string) string {
	return authorityPath + querySeparator + queryString
}
