package restusermanager

import (
	"errors"
	"fmt"
)

var ErrAlreadyExists error = errors.New("email already taken")

// ErrorResponse in case of errors
type ErrorResponse struct {
	Err    string `json:"error"`
	Status int    `json:"status,omitempty"`
}

func (e *ErrorResponse) Error() string {
	return fmt.Sprintf("%+v", e.Err)
}
