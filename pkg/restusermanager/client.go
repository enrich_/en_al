package restusermanager

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"github.com/dghubble/sling"
)

const (
	getUserByIDPath        = "/v1/%s"
	registerPath           = "/v1"
	registerFromGooglePath = "/v1/google"
	updateUserPath         = "/v1/%s"
	resetPasswdPath        = "/v1/reset_password"
	verifyResetPasswdToken = "/v1/reset_password/verify"
	signInPath             = "/v1/login"
)

//go:generate mockery -case underscore -output mocks -outpkg restusermanagermock -name Client

// Client is an interface with common methods to connect with user manager
type Client interface {
	GetUserByID(ctx context.Context, id string) (*UserResponse, error)
	Register(context.Context, RegisterUserRequest) (*UserResponse, error)
	RegisterFromGoogle(context.Context, RegisterGoogleUserRequest) (*UserResponse, error)
	UpdateUser(ctx context.Context, id string, payload UpdateUserRequest) (*UserResponse, error)
	ResetPassword(context.Context, ResetPasswordRequest) error
	VerifyResetPasswordToken(ctx context.Context, payload VerifyResetPasswordTokenRequest) (*UserResponse, error)
	SignIn(context.Context, SignInRequest) (*UserResponse, error)
}

// defaultClient represents the structure to perform authentication/authorization
type defaultClient struct {
	cfg     Config
	slinger *sling.Sling
}

// NewClient creates a new google API client
func NewClient(cfg Config, client *http.Client) Client {
	baseURL := url.URL(cfg.Address)

	slingerSvc := sling.New().Base(baseURL.String()).Client(client)

	return &defaultClient{
		cfg:     cfg,
		slinger: slingerSvc,
	}
}

func (c *defaultClient) GetUserByID(ctx context.Context, id string) (*UserResponse, error) {
	successResponse := UserResponse{}
	errResponse := ErrorResponse{}

	req, err := c.slinger.
		New().
		Get(fmt.Sprintf(getUserByIDPath, id)).
		Request()
	if err != nil {
		return nil, err
	}

	res, err := c.slinger.Do(req.WithContext(ctx), &successResponse, &errResponse)
	if err != nil {
		return nil, err
	}

	switch res.StatusCode {
	case http.StatusOK:
		return &successResponse, nil
	default:
		return nil, &errResponse
	}
}

func (c *defaultClient) Register(ctx context.Context, payload RegisterUserRequest) (*UserResponse, error) {
	successResponse := UserResponse{}
	errResponse := ErrorResponse{}

	req, err := c.slinger.
		New().
		BodyJSON(&payload).
		Post(registerPath).
		Request()
	if err != nil {
		return nil, err
	}

	res, err := c.slinger.Do(req.WithContext(ctx), &successResponse, &errResponse)
	if err != nil {
		return nil, err
	}

	switch res.StatusCode {
	case http.StatusCreated, http.StatusOK:
		return &successResponse, nil
	case http.StatusConflict:
		return nil, ErrAlreadyExists
	default:
		return nil, &errResponse
	}
}

func (c *defaultClient) RegisterFromGoogle(ctx context.Context, payload RegisterGoogleUserRequest) (*UserResponse, error) {
	successResponse := UserResponse{}
	errResponse := ErrorResponse{}

	req, err := c.slinger.
		New().
		BodyJSON(&payload).
		Post(registerFromGooglePath).
		Request()
	if err != nil {
		return nil, err
	}

	res, err := c.slinger.Do(req.WithContext(ctx), &successResponse, &errResponse)
	if err != nil {
		return nil, err
	}

	switch res.StatusCode {
	case http.StatusCreated, http.StatusOK:
		return &successResponse, nil
	default:
		return nil, &errResponse
	}
}

func (c *defaultClient) UpdateUser(ctx context.Context, id string, payload UpdateUserRequest) (*UserResponse, error) {
	successResponse := UserResponse{}
	errResponse := ErrorResponse{}

	req, err := c.slinger.
		New().
		BodyJSON(&payload).
		Put(fmt.Sprintf(updateUserPath, id)).
		Request()
	if err != nil {
		return nil, err
	}

	res, err := c.slinger.Do(req.WithContext(ctx), &successResponse, &errResponse)
	if err != nil {
		return nil, err
	}

	switch res.StatusCode {
	case http.StatusOK:
		return &successResponse, nil
	default:
		return nil, &errResponse
	}
}

func (c *defaultClient) ResetPassword(ctx context.Context, payload ResetPasswordRequest) error {
	errResponse := ErrorResponse{}

	req, err := c.slinger.
		New().
		BodyJSON(&payload).
		Post(resetPasswdPath).
		Request()
	if err != nil {
		return err
	}

	res, err := c.slinger.Do(req.WithContext(ctx), nil, &errResponse)
	if err != nil {
		return err
	}

	switch res.StatusCode {
	case http.StatusOK:
		return nil
	default:
		return &errResponse
	}
}

func (c *defaultClient) VerifyResetPasswordToken(ctx context.Context, payload VerifyResetPasswordTokenRequest) (*UserResponse, error) {
	successResponse := UserResponse{}
	errResponse := ErrorResponse{}

	req, err := c.slinger.
		New().
		BodyJSON(&payload).
		Post(verifyResetPasswdToken).
		Request()
	if err != nil {
		return nil, err
	}

	res, err := c.slinger.Do(req.WithContext(ctx), &successResponse, &errResponse)
	if err != nil {
		return nil, err
	}

	switch res.StatusCode {
	case http.StatusOK:
		return &successResponse, nil
	default:
		return nil, &errResponse
	}
}

func (c *defaultClient) SignIn(ctx context.Context, payload SignInRequest) (*UserResponse, error) {
	successResponse := UserResponse{}
	errResponse := ErrorResponse{}

	req, err := c.slinger.
		New().
		BodyJSON(&payload).
		Post(signInPath).
		Request()
	if err != nil {
		return nil, err
	}

	res, err := c.slinger.Do(req.WithContext(ctx), &successResponse, &errResponse)
	if err != nil {
		return nil, err
	}

	switch res.StatusCode {
	case http.StatusOK:
		return &successResponse, nil
	default:
		return nil, &errResponse
	}
}
