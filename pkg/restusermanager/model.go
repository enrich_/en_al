package restusermanager

import "time"

type UserResponse struct {
	ID             string    `json:"id"`
	Email          string    `json:"email"`
	FullName       string    `json:"full_name"`
	Telephone      string    `json:"telephone"`
	Address        string    `json:"address"`
	CreatedAt      time.Time `json:"created_at"`
	UpdatedAt      time.Time `json:"updated_at"`
	ThirdPartyAuth string    `json:"third_party_auth"`
}

type RegisterUserRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type RegisterGoogleUserRequest struct {
	Token string `json:"access_token"`
}

type UpdateUserRequest struct {
	Email     string `json:"email"`
	FullName  string `json:"full_name"`
	Telephone string `json:"telephone"`
	Address   string `json:"address"`
	Password  string `json:"password"`
}

type ResetPasswordRequest struct {
	Email string `json:"email"`
}

type VerifyResetPasswordTokenRequest struct {
	Token string `json:"token"`
}

type SignInRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
