## About

Please refer to the [Service](./SERVICE.md) section.

## Getting Started

### Requirements

- Golang 1.12+
- Google API
- Google OAuth2 Credentials
- Sendgrid Credentials

### Installation

#### Check Tools

In order to make all the checks in this codebase, there are some external go tools that must be installed:

```bash
$ make install-tools
```

#### Project Dependencies

```bash
$ make install-deps
```

## Building

Note: if you are using Mac, you need to make sure you have the GNU coreutils instead of the default BSD. You can install 
these using brew:
```bash
brew install parallel coreutils findutils grep bash
```

To use the latest `bash` version remember to set: `export PATH=/usr/local/bin:$PATH`

### Compile & Build Docker image

We can compile and run the service using docker.

To build the service run:

```bash
make compile build
```

This will compile the `gunhild` service in a container and create the image.

## Testing & Linting

It will run go linters and the unit tests
```bash
$ make check
```

If you prefer to run the test separately
```bash
$ make test
```

Or even if you prefer to run unit tests and visualize a detailed coverage report, instead the above command you can exec:
```bash
$ make coverage
```

## Start

Running it locally
```bash
$ cp .env.webapp.example .env.webapp
$ cp .env.usermanager.example .env.usermanager

$ make build-localdb
$ docker-compose up -d
$ make start-api
$ make start-webapp
```

Running with docker-compose
```bash
$ cp .env.webapp.example .env.webapp
$ cp .env.usermanager.example .env.usermanager

$ docker-compose -f docker-compose.deploy.yml up -d
```