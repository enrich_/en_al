# User Manager

## Context

TBD

## Constraints

### Technical

TBD

### Operational

TBD

## Quality Attributes

## Failure Case or Degraded Mode

* When MySQL is outage
* When Google OAuth API is outage

## Decisions

TBD

### Dependencies

TBD

### Diagrams

#### Services

![Diagram](./diagram.png)

## References

TBD

## Improvements

* Rewrite UI and templates: use template partials, separate CSS and JS files, etc
* Improve error handling
* Revisit the environment variables for possible renames
* Write acceptance and unit tests
* Add microservice mechanisms like circuit breakers
* Move ssesion to a cache storage, like redis or memcached
* Improve local execution scripts
* Add a CI with bitbucket-pipelines
* Add documentation



