#!/bin/bash

# Lint project

# Check required commands
command -v golangci-lint >/dev/null 2>&1 || { echo 'please install golangci-lint or use image that has it'; exit 1; }

echo "lint"
bash ./scripts/checks/lint.sh

echo "test"
bash ./scripts/checks/test.sh
