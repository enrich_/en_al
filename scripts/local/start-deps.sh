#!/bin/bash

# Start project dependencies

# Check required commands
command -v docker > /dev/null 2>&1 || { echo 'please install docker or use image that has it'; exit 1; }
command -v docker-compose > /dev/null 2>&1 || { echo 'please install docker or use image that has it'; exit 1; }

docker-compose up -d

# TODO: use wait for it
sleep 5