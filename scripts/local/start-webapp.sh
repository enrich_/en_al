#!/bin/bash

# Execute project locally

# Check required commands
command -v go > /dev/null 2>&1 || { echo 'please install go or use image that has it'; exit 1; }

echo -e "\n# Starting distribution: $1\n"
## Compile and run project

FILE=$(pwd)/.env.webapp

if [ ! -f "$FILE" ]; then
    echo "$FILE does not exist."
    exit 1
fi

read_var() {
    VAR=$(grep $1 $2 | xargs)
    IFS="=" read -ra VAR <<< "$VAR"
    echo ${VAR[1]}
}

WEBAPP_GOOGLEOAUTH2_CLIENTID=$(read_var WEBAPP_GOOGLEOAUTH2_CLIENTID $FILE) \
WEBAPP_GOOGLEOAUTH2_CLIENTSECRET=$(read_var WEBAPP_GOOGLEOAUTH2_CLIENTSECRET $FILE) \
WEBAPP_USERMANAGER_ADDRESS=$(read_var WEBAPP_USERMANAGER_ADDRESS $FILE) \
WEBAPP_COOKIEDOMAIN=$(read_var WEBAPP_COOKIEDOMAIN $FILE) \
WEBAPP_WEBAPP_SESSIONSECRET=$(read_var WEBAPP_WEBAPP_SESSIONSECRET $FILE) \
go run "$1"