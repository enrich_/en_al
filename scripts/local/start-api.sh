#!/bin/bash

# Execute project locally

# Check required commands
command -v go > /dev/null 2>&1 || { echo 'please install go or use image that has it'; exit 1; }

FILE=$(pwd)/.env.usermanager

if [ ! -f "$FILE" ]; then
    echo "$FILE does not exist."
    exit 1
fi

read_var() {
    VAR=$(grep $1 $2 | xargs)
    IFS="=" read -ra VAR <<< "$VAR"
    echo ${VAR[1]}
}

echo -e "\n# Starting distribution: $1\n"
## Compile and run project
USERMANAGER_MYSQL_CONNECTION=$(read_var USERMANAGER_MYSQL_CONNECTION $FILE) \
USERMANAGER_SENDGRIDMAILER_APIKEY=$(read_var USERMANAGER_SENDGRIDMAILER_APIKEY $FILE) \
USERMANAGER_WEBAPPDOMAIN=$(read_var USERMANAGER_WEBAPPDOMAIN $FILE) \
USERMANAGER_RESETPASSWDTOKEN_SECRET=$(read_var USERMANAGER_RESETPASSWDTOKEN_SECRET $FILE) \
go run "$1"