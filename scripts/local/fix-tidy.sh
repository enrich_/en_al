#!/bin/bash

# Prune and add missing dependencies

# Check required commands
command -v go > /dev/null 2>&1 || { echo 'please install go or use image that has it'; exit 1; }

go mod tidy
go mod verify
